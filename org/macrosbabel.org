#+TITLE: Macros de sustitución en Org Mode para Babel (LaTeX)
#+AUTHOR: Juan Manuel Macías
#+SETUPFILE: ~/Git/gnutas/html-gnutas.setup
#+SETUPFILE: ~/Git/gnutas/macros-gnutas.setup
#+OPTIONS: num:nil
#+OPTIONS: toc:nil
#+OPTIONS: tags:nil
#+OPTIONS: d:nil
#+LANGUAGE: es
#+OPTIONS: ':t
#+STARTUP: inlineimages
#+OPTIONS: todo:nil
#+STARTUP: fnlocal
#+INDEX: GNU Emacs!org-mode!macros!como comandos de Babel en la exportación a LaTeX
#+INDEX: LaTeX!babel

Cuando se trabaja en un documento de LaTeX desde el Org Mode de Emacs, desearemos en ocasiones que ciertos pasajes
cortos en lenguas extranjeras pasen a LaTeX formados con el comando típico de Babel
~\foreignlanguage{<lengua>}{<pasaje>}~. Podemos introducir este comando directamente en Org, pero si exportásemos a
otros formatos perderíamos el pasaje (o nos aparecería explícitamente el ~\foreignlanguage{}{}~) y tendríamos que andar
retocando el documento, lo cual es una lata. Para salir del paso, he descubierto que aquí son muy útiles las macros de
sustitución de Org, que permiten sustituir una cadena por "comodines" y evaluar, además, algo de código elisp en la
parte que sustituye. Ya tratamos aquí [[https://maciaschain.gitlab.io/gnutas/macrosorgmode.html][algún que otro ejemplo]] de estas macros en una pasada /gnuta/. Veamos ahora un par de
ejemplos más para la salida a LaTeX con Babel.

Podemos definir, en principio, una macro de sustitución que incluye un simple condicional: si el pasaje extranjero se exporta
a LaTeX, entonces se forma con Babel. Si es a cualquier otro formato (HTML, ODT, EPUB, etc.), se exporta tal cual. Esta
primera macro admitiría dos argumentos: el nombre de la lengua, según Babel, y el pasaje en esa lengua.

#+begin_src org
,#+TITLE:Prueba con macros de sustitución para idiomas
,#+LaTeX_Header: \usepackage[greek.ancient,spanish]{babel}

,#+MACRO: lg (eval (if (org-export-derived-backend-p org-export-current-backend 'latex) (concat "@@latex:\\foreignlanguage{@@" $1 "@@latex:}{@@" $2 "@@latex:}@@") $2))

Esto es una prueba de texto griego con la primera macro: {{{lg(greek,ἄνθρωπος)}}}
#+end_src

Si exportamos a LaTeX obtendremos, entonces, =\foreignlanguage{greek}{ἄνθρωπος}=. Así nos aseguramos de que TeX cortará
por guiones (si se presentase el contexto) correctamente esta palabra, según los patrones de guionado y silabación para
griego antiguo, tal y como hemos definido en las opciones de Babel al cargar =greek= con el atributo =ancient=[fn:23].

Otra posible macro a definir tendría sólo un argumento (la palabra o pasaje en lengua extranjera). Eso sí, tendríamos
que definir tantas macros como lenguas quisiésemos cargar en nuestro documento.

#+begin_src org
,#+MACRO: gr (eval (if (org-export-derived-backend-p org-export-current-backend 'latex) (concat "@@latex:\\foreignlanguage{greek}{@@" $1 "@@latex:}@@") $1))

... Y esto es otro ejemplo con la segunda macro: {{{gr(ἄνθρωπος)}}}
#+end_src

Así, si hubiesemos cargado (pongo por caso) también latín y alemán, definiríamos estas dos más:

#+begin_src org
,#+TITLE:Prueba con macros de sustitución para idiomas
,#+LaTeX_Header: \usepackage[greek.ancient,latin,german,spanish]{babel}

,#+MACRO: lat (eval (if (org-export-derived-backend-p org-export-current-backend 'latex) (concat "@@latex:\\foreignlanguage{latin}{@@" $1 "@@latex:}@@") $1))
,#+MACRO: ger (eval (if (org-export-derived-backend-p org-export-current-backend 'latex) (concat "@@latex:\\foreignlanguage{german}{@@" $1 "@@latex:}@@") $1))

... Y otro ejemplo más con un pasaje en latín: {{{lat(Delenda est Cartago)}}}

o en alemán: {{{ger(Goldner Schlaf, nur dessen Herz zufrieden)}}}
#+end_src

[fn:23] Recordemos que Babel ofrece tres posibles atributos para la opción greek: monotónico, politónico y griego antiguo
* ∞

#+begin_export html
<div>
<p>Publicado: 27/08/2019 </p>

<p>Última actualización: 27/08/2019 </p>
</div>
<div>
<hr />
<p>
<a href="index.html">Índice general</a>
</p>
<p>
<a href="acerca-de.html">Acerca de...</a>
</p>
<p>
Esta obra está bajo una <a rel="license" href="http://creativecommons.org/licenses/by-nc/4.0/">licencia de Creative Commons Reconocimiento-NoComercial 4.0 Internacional</a>.
</p>
</div>
#+end_export
