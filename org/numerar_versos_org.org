#+TITLE: Mostrar numeración de versos en un bloque VERSE de Org Mode
#+AUTHOR: Juan Manuel Macías
#+SETUPFILE: ~/Git/gnutas/html-gnutas.setup
#+SETUPFILE: ~/Git/gnutas/macros-gnutas.setup
#+OPTIONS: num:nil
#+OPTIONS: toc:nil
#+OPTIONS: tags:nil
#+OPTIONS: d:nil
#+LANGUAGE: es
#+OPTIONS: ':t
#+STARTUP: inlineimages
#+OPTIONS: todo:nil
#+STARTUP: fnlocal
#+INDEX: GNU Emacs!org-mode!numerar versos en un bloque verse

#+BIND: org-export-filter-final-output-functions (filtro-html-misc)
#+begin_src emacs-lisp :exports results :results none
 ;; Para html
  (defun filtro-html-misc (texto backend info)
    "modifica epígrafe de las notas y añade un escalado menor a los
      números romanos, para emular algo parecido a versalitas"
    (when (org-export-derived-backend-p backend 'html)
      (replace-regexp-in-string ">Nota al pie de p&aacute;gina:"  ">Notas:"
      (replace-regexp-in-string "<p>Última actualización: "  (concat "<p>Última actualización: " (format "%s" (format-time-string "%d/%m/%y")))
      (replace-regexp-in-string "v-\\([IVXLCDM]+\\)-v"  "<font style=\"font-size:smaller;\">\\1</font>" texto)))))
#+end_src

Gnu Emacs (como cualquier otro editor de texto que se precie, por otra parte) puede
mostrar u ocultar los números de línea lógicos del documento. Esto tiene su utilidad para
programación, y yo suelo usar, si ando en esos menesteres, el =display-line-numbers-mode=,
cuya activación o desactivación tengo anclada a una secuencia de teclas. Hace bien su
trabajo y no le pido más. Por supuesto, hay muchos modos más para numeración de líneas,
incluso numeración relativa, pero ninguno (ni siquiera =display-line-numbers=) me resulta
satisfactorio si de lo que se trata es de numerar los versos de un poema. Y ante esta
carencia, me dediqué hace no mucho a escribir esta función, con la idea de que realice lo
siguiente:

1. Si estamos en un bloque =verse= de Org Mode (el lugar más idóneo para poner un poema en
   Emacs) y llamamos a la función, los versos se numerarán en el margen (mediante
   /overlays/ temporales, que es el mismo procedimiento que emplean los otros modos para
   numerar las líneas) en secuencia de cinco, pero sin contar el primer verso. Y
2. Es importante que en el cómputo de versos no se tenga en cuenta las líneas blancas que
   separan las estrofas. Ahí es una de las cosas en que la numeración "tradicional" de
   líneas en Emacs me falla, ya que numera todas las líneas reales, ya sean blancas o con
   contenido.

Como se ve, es la numeración clásica de los versos, algo que en LaTeX se puede conseguir
fácilmente. De hecho, [[https://maciaschain.gitlab.io/gnutas/orglatex.html][en esta /GNUta/​]] explico cómo exporto versos desde Org a LaTeX y que
queden en su destino, automáticamente, numerados. Pero mientras trabajo en Emacs, en el
paraíso del texto plano, también me resulta práctico de cuando en vez contar con ese tipo
de numeración, especialmente en mi traducción de la /Odisea/, /work in progress/. Echemos
un vistazo rápido a cómo quedó, finalmente, mi código.

* Función para mostrar u ocultar la numeración de versos
  :PROPERTIES:
  :CUSTOM_ID: Función para mostrar u ocultar la numeración de versos
  :END:

Primero, necesité definir antes esta pequeña función que avanza cuatro líneas pero ignorando las blancas. Como no me
servía el comando =forward-line= (que no ignora las bancas), me inventé uno nuevo /ad hoc/, que repite cuatro veces una
búsqueda con expresión regular:

#+begin_src emacs-lisp :exports code
(defun cuatro-versos-no-vacia ()
    (dotimes (numero 4)
      (re-search-forward "^.+" nil t)))
#+end_src

Nuestra función para numerar los versos, que expongo a continuación, tiene un esquema muy simple: se define un contador
como una variable local, se avanza líneas y cada avance se añade un overlay en el margen:

#+begin_src emacs-lisp :exports code
  (defun numera-versos ()
    (interactive)
    (setq left-margin-width 4)
    (set-window-buffer (selected-window) (current-buffer))
    (save-excursion
      (save-restriction
	(org-narrow-to-block)
	(goto-char (point-min))
	(end-of-line)
	(let
	    ((numverso 0))
	  (save-excursion
	    (while
		(re-search-forward "^.+" nil t)
	      (cuatro-versos-no-vacia)
	      (let
		  ((ov (make-overlay (point) (point))))
		(overlay-put ov 'overlay-num-verso t)
		 ;; necesario para evitar el overlay en la línea que
		 ;; cierra el bloque
		(unless (looking-back "#\\+end_verse\\|#\\+END_VERSE")
		  (overlay-put ov 'before-string
			       (propertize " "
					   'display
					   `((margin left-margin)
					     ,(propertize
					       (format "%s" (number-to-string (setf numverso (+ numverso 5))))
					       'face 'linum))))))
	      (forward-line 1)))))))
#+end_src


Ya sólo nos quedaría definir esta otra función que oculte los números de verso y restituya los márgenes normales:

#+begin_src emacs-lisp :exports code
(defun elimina-num-versos ()
    (interactive)
    (remove-overlays nil nil 'overlay-num-verso t)
    (setq left-margin-width 0)
    (set-window-buffer (selected-window) (current-buffer)))
#+end_src

Y podemos ya anclar las funciones a un atajo de teclado (por ejemplo, =M-j= o el que se nos antoje). Si, además, queremos guardar la cortesía con nosotros mismos, y
que se nos recuerde que este código sólo tiene sentido usarlo en un bloque =verse=, podemos incluir un condicional y un
mensaje de error:

#+begin_src emacs-lisp :exports code
(global-set-key (kbd "M-j")
		  '(lambda ()
			   (interactive)
			   (save-excursion
			     (save-restriction
			       (org-narrow-to-element)
			       (goto-char (point-min))
			       (if (not (looking-at-p "#\\+begin_verse"))
				   (error "No es un bloque VERSE..."))
			       (numera-versos))))
#+end_src

[[./images/num-versos-org.gif]]

* ¿Y si queremos saltar a un verso concreto?
  :PROPERTIES:
  :CUSTOM_ID: ¿Y si queremos saltar a un verso concreto?
  :END:

Pues, como /bonus track/, y ya venido arriba, también se me ocurrió esta otra función, muy útil, que nos preguntará en
el minibúfer a qué número queremos saltar:

#+begin_src emacs-lisp :exports code
(defun localiza-verso ()
    (interactive)
    (save-restriction
      (org-narrow-to-block)
      (let
	  ((numero (read-number "ir a verso...")))
	(goto-char (point-min))
	(end-of-line)
	(dotimes (x numero)
	  (re-search-forward "^." nil t)))))
#+end_src

* Actualización de 25/08/20: un modo menor
  :PROPERTIES:
  :CUSTOM_ID: Actualización de 25/08/20: un modo menor
  :END:

Aprovechando las funciones anteriores, podemos definir un modo menor para que los versos
se vayan numerando a medida que escribimos. Primero, definimos esta función, que elimina
la numeración y la vuelve a mostrar para tenerla actualizada:

#+begin_src emacs-lisp :exports code
(defun inserta-numero-versos-org ()
  (save-excursion
    (save-restriction
      (org-narrow-to-element)
      (goto-char (point-min))
      (if (not (looking-at-p "\s*#\\+begin_verse\\|\s*#\\+BEGIN_VERSE"))
	  (error "No es un bloque VERSE...")
	(elimina-num-versos)
	(numera-versos)))))
#+end_src

Y la idea es que con nuestro modo menor la función anterior se evalúe cada vez que
introducimos una pulsación de teclado:

#+begin_src emacs-lisp :exports code
(define-minor-mode num-versos-mode
  "Inserta y actualiza números de verso de cinco en cinco en un
bloque `verse' de Org, sin contar las líneas blancas"
  :init-value nil
  :lighter ("versos")
  (if num-versos-mode
      (add-hook 'post-self-insert-hook #'inserta-numero-versos-org nil 'local)
    (remove-hook 'post-self-insert-hook #'inserta-numero-versos-org 'local)
    (elimina-num-versos)))
#+end_src

Por último, para muestra un botón, con un soneto de Sor Juana:

#+begin_center
#+begin_export html
<iframe src="https://player.vimeo.com/video/451577490" width="640" height="459" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>
<p><a href="https://vimeo.com/451577490">Un modo menor para numerar los versos en Org mientras se escribe</a> from <a href="https://vimeo.com/user70979426">Juan Manuel Mac&iacute;as Cha&iacute;n</a> on <a href="https://vimeo.com">Vimeo</a>.</p>
#+end_export
#+end_center

* ∞
  :PROPERTIES:
  :CUSTOM_ID: ∞
  :END:

#+begin_export html
<div>
<p>Publicado: 22/10/2019 </p>

<p>Última actualización:   </p>
</div>
<div>
<hr />
<p>
<a href="index.html">Índice general</a>
</p>
<p>
<a href="acerca-de.html">Acerca de...</a>
</p>
<p>
Esta obra está bajo una <a rel="license" href="http://creativecommons.org/licenses/by-nc/4.0/">licencia de Creative Commons Reconocimiento-NoComercial 4.0 Internacional</a>.
</p>
</div>
#+end_export
