#+TITLE: Eliminar en Dired los espacios en los nombres de ficheros
#+AUTHOR: Juan Manuel Macías
#+SETUPFILE: ~/Git/gnutas/html-gnutas.setup
#+SETUPFILE: ~/Git/gnutas/macros-gnutas.setup
#+OPTIONS: num:nil
#+OPTIONS: toc:nil
#+OPTIONS: tags:nil
#+OPTIONS: d:nil
#+LANGUAGE: es
#+OPTIONS: ':t
#+STARTUP: inlineimages
#+OPTIONS: todo:nil
#+STARTUP: fnlocal

#+INDEX: GNU Emacs!Dired!eliminar espacios en nombres de ficheros

#+BIND: org-export-filter-final-output-functions (filtro-html-misc)
#+begin_src emacs-lisp :exports results :results none
 ;; Para html
  (defun filtro-html-misc (texto backend info)
    "modifica epígrafe de las notas y añade un escalado menor a los
      números romanos, para emular algo parecido a versalitas"
    (when (org-export-derived-backend-p backend 'html)
      (replace-regexp-in-string ">Nota al pie de p&aacute;gina:"  ">Notas:"
      (replace-regexp-in-string "<p>Última actualización: "  (concat "<p>Última actualización: " (format "%s" (format-time-string "%d/%m/%y")))
      (replace-regexp-in-string "v-\\([IVXLCDM]+\\)-v"  "<font style=\"font-size:smaller;\">\\1</font>" texto)))))
#+end_src

Los archivos nombrados con espacios resultan ---es sabido--- de lo más incómodo a la hora de bregar en operaciones a
nivel de terminal. En mi trabajo suelo encontrarme demasiado a menudo con directorios enteros plagados de ficheros así,
aunque por suerte contamos con mil y un recursos para reparar tal desaliño de nombres, tanto en la consola como en los
navegadores gráficos de archivos. Como yo casi nunca salgo del navegador de archivos que viene con Emacs, Dired, se me
ocurrió hace poco escribir esta sencilla función para sustituir los espacios de los ficheros que tenemos marcados allí
por el típico y recurrente guión bajo. La función tiene una estructura muy similar a [[https://maciaschain.gitlab.io/gnutas/renombrar_numerar_dired.html][esta otra]] que describimos aquí, es
decir, saca provecho de intercalar dos listas (archivos viejos con archivos nuevos) gracias a la función =-interleave=
del paquete =dash.el=. Del resto se ocupa el comando de =bash= =mv=.

Nuestra funcioncilla quedaría, entonces, tal que así:

#+begin_src emacs-lisp :exports code
(defun guiones-dired ()
  (interactive)
  (let*
      ((lista-archivos (dired-get-marked-files))
       (archivos (mapcar (lambda (x)
			   (replace-regexp-in-string ".+/" ""  x))
			 lista-archivos))
       (archivos-nuevos (mapcar (lambda (archivo)
				  (replace-regexp-in-string "\s" "_" archivo))
				archivos)))
    (setq archivos (mapcar (lambda (item) (replace-regexp-in-string "\\(.+\\)" "mv '\\1' " item)) archivos))
    (setq archivos-nuevos (mapcar (lambda (item) (replace-regexp-in-string "\\(.+\\)" "\\1\n" item)) archivos-nuevos))
    (setq lista-archivos-res (-interleave archivos archivos-nuevos))
    (shell-command
     (mapconcat 'identity lista-archivos-res " "))
    (revert-buffer)))
#+end_src

Y como un gif vale más que mil palabras:

[[./images/guiones_dired.gif]]

* ∞

#+begin_export html
<div>
<p>Publicado: 04/01/20 </p>

<p>Última actualización: </p>
</div>
<div>
<hr />
<p>
<a href="index.html">Índice general</a>
</p>
<p>
<a href="acerca-de.html">Acerca de...</a>
</p>
<p>
Esta obra está bajo una <a rel="license" href="http://creativecommons.org/licenses/by-nc/4.0/">licencia de Creative Commons Reconocimiento-NoComercial 4.0 Internacional</a>.
</p>
</div>
#+end_export
