#+TITLE: Información rápida de un carácter en Emacs (con nombre Unicode y código)
#+AUTHOR: Juan Manuel Macías
#+SETUPFILE: ~/Git/gnutas/html-gnutas.setup
#+SETUPFILE: ~/Git/gnutas/macros-gnutas.setup
#+OPTIONS: num:nil
#+OPTIONS: toc:nil
#+OPTIONS: tags:nil
#+OPTIONS: d:nil
#+LANGUAGE: es
#+OPTIONS: ':t
#+STARTUP: inlineimages
#+OPTIONS: todo:nil
#+STARTUP: fnlocal

#+INDEX: Unicode!extraer el código hexadecimal de un carácter

#+BIND: org-export-filter-final-output-functions (filtro-html-misc)
#+begin_src emacs-lisp :exports results :results none
 ;; Para html
  (defun filtro-html-misc (texto backend info)
    "modifica epígrafe de las notas y añade un escalado menor a los
      números romanos, para emular algo parecido a versalitas"
    (when (org-export-derived-backend-p backend 'html)
      (replace-regexp-in-string ">Nota al pie de p&aacute;gina:"  ">Notas:"
      (replace-regexp-in-string "<p>Última actualización: "  (concat "<p>Última actualización: " (format "%s" (format-time-string "%d/%m/%y")))
      (replace-regexp-in-string "v-\\([IVXLCDM]+\\)-v"  "<font style=\"font-size:smaller;\">\\1</font>" texto)))))
#+end_src

La función =describe-char= de Emacs es tremendamente útil. Si la llamamos interactivamente nos desplegará una ventana
contigua con una información prolija del carácter sobre el que tenemos situado el cursor. Lo que ocurre es que esa
información puede llegar a ser muchas veces demasiado densa, cuando lo que queremos tan sólo es conocer o recordar unos
pocos datos. En mi caso, me basta y me sobra con:

a. Nombre canónico Unicode
b. Código hexadecimal del carácter
c. y, ya puestos y si el carácter es complejo, la descomposición canónica y los dos códigos simples

Así que no me ha sido muy difícil escribir esta sencilla función. Muestra en el /echo-area/ del minibúfer los datos
anteriores y, por si queremos hacer uso de ellos más tarde, los guarda en el portapapeles de Emacs, el /kill-ring/. La
función quedó tal que así:

#+begin_src emacs-lisp :exports code
(defun nombre-caracter-actual ()
  "muestra el nombre Unicode del carácter con su código actual en
el minibúfer y lo copia en el kill-ring"
  (interactive)
  (let*
      ((nombre-caracter (get-char-code-property (char-after (point)) 'name))
       ;; pasamos el código a hexadecimal
       (codigo (format "#%x" (char-after (point))))
       (desc (get-char-code-property (char-after (point)) 'decomposition))
       (nombre-codigo (concat
		       nombre-caracter
		       " / "
		       codigo
		       " / descomp: "
		       desc
		       "\s"
		       (mapconcat (lambda (cod)
				    (format "#%x" cod))
				  desc "\s+\s"))))
    (kill-new nombre-codigo)
    (message nombre-codigo)))
#+end_src

Y sigue una breve explicación. Primero declaramos una lista de variables locales que deben tenerse en cuenta por su
orden, así que usamos la versión estrellada de la [[https://maciaschain.gitlab.io/gnutas/let.html][expresión let]]:

- =nombre-caracter= nos devuelve como cadena el nombre canónico Unicode. Para ello echamos mano de la función
  =get-char-code-property= llamando a la propiedad =name=
- =codigo= nos da como cadena el código hexadecimal. Como Emacs trabaja con códigos decimales, debemos convertirlo a
  hexadecimal. Véase, para más información, la documentación de la expresión =format=
- =desc= nos da el carácter compuesto por combinación
- =nombre-codigo= concatena todas las variables. Para sacar el código de los caracteres combinatorios usamos una
  expresión [[https://maciaschain.gitlab.io/gnutas/mapconcat.html][=mapconcat=]], ya que la variable =desc= nos da una lista de dos códigos decimales.

Con nuestra función ya evaluada, si la llamamos con el cursor en, por ejemplo, la letra a con acento =á=, en el
minibúfer nos aparecerá: =LATIN SMALL LETTER A WITH ACUTE / #e1 / descomp: á #61 + #301=. Los códigos hexadecimales hay
que leerlos en Emacs sustituyendo el signo de la almohadilla =#= por los ceros necesarios. De tal forma que tenemos:

1. =#e1=, el código del carácter precompuesto, que equivale a =U+00E1=
2. =#61 + #301=, los dos códigos combinatorios, que equivalen a =U+0061= (la letra =a=) y =U+=0301= (el acento agudo de combinación).
* ∞

#+begin_export html
<div>
<p>Publicado: 15/01/20 </p>

<p>Última actualización: </p>
</div>
<div>
<hr />
<p>
<a href="index.html">Índice general</a>
</p>
<p>
<a href="acerca-de.html">Acerca de...</a>
</p>
<p>
Esta obra está bajo una <a rel="license" href="http://creativecommons.org/licenses/by-nc/4.0/">licencia de Creative Commons Reconocimiento-NoComercial 4.0 Internacional</a>.
</p>
</div>
#+end_export
