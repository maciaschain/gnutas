#+TITLE: Enlazado léxico en Elisp: una breve explicación
#+AUTHOR: Juan Manuel Macías
#+SETUPFILE: ~/Git/gnutas/html-gnutas.setup
#+SETUPFILE: ~/Git/gnutas/macros-gnutas.setup
#+OPTIONS: num:nil
#+OPTIONS: toc:nil
#+OPTIONS: tags:nil
#+OPTIONS: d:nil
#+LANGUAGE: es
#+OPTIONS: ':t
#+STARTUP: inlineimages
#+OPTIONS: todo:nil
#+STARTUP: fnlocal

#+INDEX: GNU Emacs!Elisp!lexical-binding

#+BIND: org-export-filter-final-output-functions (filtro-html-misc)
#+begin_src emacs-lisp :exports results :results none
 ;; Para html
  (defun filtro-html-misc (texto backend info)
    "modifica epígrafe de las notas y añade un escalado menor a los
      números romanos, para emular algo parecido a versalitas"
    (when (org-export-derived-backend-p backend 'html)
      (replace-regexp-in-string ">Nota al pie de p&aacute;gina:"  ">Notas:"
      (replace-regexp-in-string "<p>Última actualización: "  (concat "<p>Última actualización: " (format "%s" (format-time-string "%d/%m/%y")))
      (replace-regexp-in-string "v-\\([IVXLCDM]+\\)-v"  "<font style=\"font-size:smaller;\">\\1</font>" texto)))))
#+end_src

En la última versión estable de GNU Emacs, la 27.1, viene activada por defecto la opción
=lexical-binding=, y eso es algo que conviene tener en cuenta a la hora de escribir
nuestro código en Elisp, principalmente en lo que atañe al alcance de las variables
locales. Es sabido que el código puede dar resultados diversos dependiendo si activamos el
enlazado léxico o el dinámico. O incluso suceder que lo que funciona en el primero,
devuelve error en el segundo. En cualquier caso, esta elección de los desarrolladores de
Emacs me parece una noticia feliz, dado que el enlazado léxico siempre nos aporta mayor
flexibilidad en muchos escenarios, y ya no será necesario activarlo como variable local al
principio de nuestros ficheros, como cuando queríamos echar mano de esa modalidad.

Aprovechando la novedad, he hecho este breve vídeo con una muy sencilla (eso espero)
explicación de las diferencias entre ambos enlazados, y cómo se comportan las variables
locales en uno y en otro.

#+begin_center
#+begin_export html
<iframe src="https://player.vimeo.com/video/455167304" width="640" height="360" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>
<p><a href="https://vimeo.com/455167304">El enlazado l&eacute;xico en Elisp (una breve explicaci&oacute;n)</a> from <a href="https://vimeo.com/user70979426">Juan Manuel Mac&iacute;as Cha&iacute;n</a> on <a href="https://vimeo.com">Vimeo</a>.</p>
#+end_export
#+end_center
* ∞
  :PROPERTIES:
  :CUSTOM_ID: ∞
  :END:

#+begin_export html
<div>
<p>Publicado: 07/09/20 </p>

<p>Última actualización: </p>
</div>
<div>
<hr />
<p>
<a href="index.html">Índice general</a>
</p>
<p>
<a href="acerca-de.html">Acerca de...</a>
</p>
<p>
Esta obra está bajo una <a rel="license" href="http://creativecommons.org/licenses/by-nc/4.0/">licencia de Creative Commons Reconocimiento-NoComercial 4.0 Internacional</a>.
</p>
</div>
#+end_export
