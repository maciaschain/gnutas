#+TITLE: Explicación rápida de una expresión ~let~ en Elisp
#+AUTHOR: Juan Manuel Macías
#+SETUPFILE: ~/Git/gnutas/html-gnutas.setup
#+OPTIONS: num:nil
#+OPTIONS: toc:nil
#+OPTIONS: tags:nil
#+OPTIONS: d:nil
#+LANGUAGE: es
#+OPTIONS: ':t
#+STARTUP: inlineimages
#+OPTIONS: todo:nil
#+STARTUP: fnlocal
#+INDEX: GNU Emacs!Elisp!let

Emacs-Lisp, el dialecto de Lisp de GNU Emacs, es increíblemente potente y versátil. Gracias a él podemos extender y
programar nuestro polifacético editor casi hasta el infinito, y todo ello al vuelo, en tiempo real. Conviene tener
presente tres cosas esenciales que son el alma de los Lisp:

1. La frontera entre datos y código es nebulosa, lo cual es bastante productivo;
2. Los operadores preceden siempre a los operandos. Así, la expresión ~(+ 2 3)~ equivale a ~2 + 3~;
3. Es el paraíso y el parque natural de los paréntesis, que crecen y se reproducen por doquier encerrando lo que
   conocemos como "expresiones". El primer término de una expresión (en el fondo, el primer elemento de una lista) es el
   que manda, el operador. Y las expresiones pueden incluirse dentro de otras, y éstas dentro de otras más grandes, etc.

Cuando estamos aprendiendo a programar en Elisp, una expresión utilísima, casi ubícua en toda función que se precie y
que conviene conocer muy pronto, es ~let~. Mediante ~let~ podemos definir una lista de variables locales con sus
respectivos valores asignados, que sólo tendrán efecto dentro de la propia expresión ~let~. Dichas variables serán
llamadas a continuación a fin de producir eventos. La estructura de una expresión ~let~ es bien simple: primero, la
lista de variables (/varlist/). A ello le sigue el cuerpo de la expresión (/body/), dentro del cual podremos construir a
nuestro gusto. Se puede entender en un periquete con el siguiente ejemplo.

Definimos una función interactiva (es decir, que pueda ser llamada mediante la típica secuencia ~M-x nombre-función RET~
o por un atajo de teclas asignado al nombre de la función). La llamamos ~cual-es~. El cuerpo de nuestra función consiste
en una expresión ~let~, que contiene lo siguiente:

a. La lista de variables, con dos variables, "x" e "y", a las que asignamos como valores dos cadenas de texto (las
   partes de un refrán);
b. El cuerpo de la expresión ~let~, que consiste en una expresión condicional donde se nos hace una pregunta y ha de
   darse esta premisa: si la respuesta es igual a la variable "x", se nos responde con la variable "y", más una
   coletilla. Si no, se nos da una respuesta de "no correcto".

#+begin_src emacs-lisp :exports code
(defun cual-es ()
"un ejemplo refranístico con let"
  (interactive)
  ;; aquí empieza el cuerpo de la función que contiene la expresión let
  (let
      ;; nuestra preciosa lista de variables
      ( (x "no por mucho madrugar") (y "amanece más temprano") )
    ;; nuestra apolínea expresión condicional
    (if
	(equal x (read-from-minibuffer "¿Cuál es la contraseña?")) ; si x es igual a lo que escribimos como respuesta en el minibúfer
	(message "%s: es correcta la contraseña" y) ; la respuesta del minibúfer
      (message "no es correcta la contraseña, vuelva usted mañana")))) ; lo que nos respondería si x no es igual
#+end_src

Y, como un /gif/ vale más que mil palabras:

[[./images/let.gif]]

* ∞

#+begin_export html
<div>
<p>Publicado: 16/07/2019 </p>

<p>Última actualización: 16/07/2019 </p>
</div>
<div>
<hr />
<p>
<a href="index.html">Índice general</a>
</p>
<p>
<a href="acerca-de.html">Acerca de...</a>
</p>
<p>
Esta obra está bajo una <a rel="license" href="http://creativecommons.org/licenses/by-nc/4.0/">licencia de Creative Commons Reconocimiento-NoComercial 4.0 Internacional</a>.
</p>
</div>
#+end_export
