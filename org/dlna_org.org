# -*- org-confirm-babel-evaluate: nil; -*-
:PROPERTIES:
:ID:       f31ceec8-51d3-450a-aea1-732754b13ada
:END:
#+TITLE: Reproducir contenido dlna en Emacs mediante un enlace personalizado de Org Mode
#+AUTHOR: Juan Manuel Macías
#+SETUPFILE: ~/Git/gnutas/html-gnutas.setup
#+SETUPFILE: ~/Git/gnutas/macros-gnutas.setup
#+OPTIONS: num:nil
#+OPTIONS: toc:nil
#+OPTIONS: tags:nil
#+OPTIONS: d:nil
#+LANGUAGE: es
#+OPTIONS: ':t
#+STARTUP: inlineimages
#+OPTIONS: todo:nil
#+STARTUP: fnlocal

#+INDEX: simple-dlna-browser
#+INDEX: GNU Emacs!org-mode!enlaces personalizados!enlace para reproducir contenido dlna

#+INCLUDE: /home/juanmanuel/Git/gnutas/lisp.org

Llevo desde hace tiempo intentando hacer de Emacs una suerte de "centro multimedia" en
texto plano. O, para ir afinando más: almacenar en documentos de Org Mode [[https://gnutas.juanmanuelmacias.com/enlaces_personalizados2.html][enlaces]] a
contenido de vídeo y audio tanto locales como en la red. Pero lo que me faltaba es poder
definir un tipo de enlace que fuese capaz de entender al servidor dlna que tengo
corriendo en mi raspberry mediante minidlna. Finalmente, y gracias al excelente /script/
[[https://javier.io/blog/en/2016/01/22/simple-upnp-dlna-browser.html][Simple dlna browser]] de Javier López he conseguido algo que funcione de manera bastante
decente, así que paso sin más preámbulos a comentar la faena.

Lo primero que necesitamos, por supuesto, es descargar el /script/ desde la página de
[[https://github.com/javier-lopez/learn/blob/master/sh/tools/simple-dlna-browser#start-of-content][GitHub]] del autor y guardarlo en una ruta accesible, tras haberle dado los habituales
permisos de ejecución. A fin de que el /script/ encuentre y reconozca el servidor dlna
activo y realice las transacciones pertinentes, tendremos que instalar también el programa
=socat= (yo lo instalé en Arch con un simple =sudo pacman -S socat=).

Y, nada, aquí va la primera versión de mi enlace personalizado, que llama externamente al
reproductor vlc mediante =xargs= para reproducir el contenido multimedia:

#+begin_src emacs-lisp :exports code
(org-link-set-parameters
 "dlna-cutre"
 :follow (lambda (ruta)
	   (let
	       ((comando (concat "~/Scripts/./simple-dlna-browser.sh " "\""  ruta "\""  " | xargs vlc")))
	     (start-process-shell-command comando nil comando)))
 :face '(:foreground "green4" :weight bold :underline t))
#+end_src

Si en cualquier documento de Org escribimos un enlace con la etiqueta =dlna-cutre:=
seguida del (simplemente) nombre del archivo multimedia alojado en el servidor (no hace
falta la extensión: =*.mkv=, =*.avi=, etc.), y lo activamos mediante la típica orden de
Org =org-open-at-point=, lo esperable es que se nos abra el reproductor vlc (es el que
usamos en este ejemplo) con el contenido a reproducir.

#+CAPTION: Probando unos cuantos enlaces a vídeos de mi servidor dlna
#+NAME: fig1
[[./images/enlaces-dlna.png]]

Por supuesto, podemos hacer las cosas aún más emacscéntricas y que el archivo se
reproduzca mediante =emms= (Emacs Multimedia System). En esta segunda versión de nuestro
enlace personalizado ya no es necesario llamar a vlc mediante =xargs= con una
tubería de bash.

#+begin_src emacs-lisp :exorts code
  (org-link-set-parameters
   "dlna-cutre-emms"
   :follow (lambda (ruta)
	     (let
		 ((comando (concat "~/Scripts/./simple-dlna-browser.sh " "\""  ruta "\"")))
	       ;; hay que asegurarse de eliminar el búfer con la
	       ;; información de salida del comando
	       (when (get-buffer "*dlna*")
		 (kill-buffer "*dlna*"))
	       (call-process-shell-command comando nil "*dlna*" t)
	       (emms-play-url (save-window-excursion
				(switch-to-buffer "*dlna*")
				(goto-char (point-min))
				(re-search-forward "\\(http:.+\\)")
				(match-string 1)))))
   :face '(:foreground "green4" :weight bold :underline t))
#+end_src

Aquí, un breve vídeo de prueba.

#+begin_center
#+begin_export html
<iframe src="https://player.vimeo.com/video/482952114" width="640" height="360" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>
<p><a href="https://vimeo.com/482952114">Reproducir contenido dlna en Emacs mediante un enlace personalizado de Org Mode (test)</a> from <a href="https://vimeo.com/user70979426">Juan Manuel Mac&iacute;as Cha&iacute;n</a> on <a href="https://vimeo.com">Vimeo</a>.</p>
#+end_export
#+end_center

* ∞
  :PROPERTIES:
  :CUSTOM_ID: ∞
  :END:

#+begin_export html
<div>
<p>Publicado: 24/11/20 </p>

<p>Última actualización: </p>
</div>
<div>
<hr />
<p>
<a href="index.html">Índice general</a>
</p>
<p>
<a href="acerca-de.html">Acerca de...</a>
</p>
<p>
Esta obra está bajo una <a rel="license" href="http://creativecommons.org/licenses/by-nc/4.0/">licencia de Creative Commons Reconocimiento-NoComercial 4.0 Internacional</a>.
</p>
</div>
#+end_export
