#+TITLE: Exportar con comillas tipográficas en Org Mode
#+AUTHOR: Juan Manuel Macías
#+SETUPFILE: ~/Git/gnutas/html-gnutas.setup
#+OPTIONS: num:nil
#+OPTIONS: toc:nil
#+OPTIONS: tags:nil
#+OPTIONS: d:nil
#+LANGUAGE: es
#+OPTIONS: ':t
#+STARTUP: inlineimages
#+OPTIONS: todo:nil
#+STARTUP: fnlocal
#+INDEX: GNU Emacs!org-mode!exportación!comillas tipográficas


Si tenemos un texto en Org con las comillas rectas típicas de máquina de escribir (=""=), que son siempre tan cómodas de
teclear como inapropiadas para la tipografía, podemos enmendar el pecadillo automáticamente en el proceso de exportación
a otros formatos (HTML, LaTeX, ODT, etc). Basta con incluir en la cabecera del documento la opción apropiada:

#+begin_src org :exports code
,#+OPTIONS: ':t
#+end_src

Y entonces Org exportará con las comillas correctas para la lengua que hayamos expecificado antes, por ejemplo español:

#+begin_src org :exports code
,#+LANGUAGE: es
#+end_src

Es decir, transformará las comillas dobles en las angulares o latinas («»), que son las que prescribe la ortotipografía
española para las comillas de primer nivel, y no las altas (“”), que corresponderían al segundo nivel y serían la
traducción de las rectas simples de nuestro texto de origen.

#+begin_src org :exports code
"las comillas 'correctas' para español son las latinas"

Sin embargo, las comillas rectas se conservarán en los ="literales"=
y también en los bloques de código:

,#+begin_src emacs-lisp
(add-to-list 'load-path "~/.emacs.d/lisp/")
,#+end_src
#+end_src

[[file:images/comillas.png]]

Como /bonus track/ nos convertirá también el signo de la comilla simple, que habremos usado (mal) como apóstrofe, por el
apóstrofe correcto.

Ah, y no sólo en Org. En LaTeX las comillas también pueden ser muy listas [[https://revistacuadernoatico.com/apuntestipograficos/2019/01/18/comillas-tipograficas-y-latex/][gracias al paquete ~csquotes~]].

* ∞
  :PROPERTIES:
  :ID:       fb5658c6-e9ac-4e35-8950-72cdcd3413e7
  :PUBDATE:  <2019-07-14 dom 19:46>
  :END:

#+begin_export html
<div>
<p>Publicado: 17/05/2019 </p>

<p>Última actualización: 17/05/2019 </p>
</div>
<div>
<hr />
<p>
<a href="index.html">Índice general</a>
</p>
<p>
<a href="acerca-de.html">Acerca de...</a>
</p>
<p>
Esta obra está bajo una <a rel="license" href="http://creativecommons.org/licenses/by-nc/4.0/">licencia de Creative Commons Reconocimiento-NoComercial 4.0 Internacional</a>.
</p>
</div>
#+end_export
