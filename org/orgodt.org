# -*- org-confirm-babel-evaluate: nil; -*-
#+TITLE: Exportar de Org a ~odt~ (algunos consejos)
#+AUTHOR: Juan Manuel Macías
#+SETUPFILE: ~/Git/gnutas/html-gnutas.setup
#+OPTIONS: num:nil
#+OPTIONS: toc:nil
#+OPTIONS: tags:nil
#+OPTIONS: d:nil
#+LANGUAGE: es
#+OPTIONS: ':t
#+STARTUP: inlineimages
#+OPTIONS: todo:nil
#+STARTUP: fnlocal
#+INDEX: GNU Emacs!org-mode!exportación!odt!estilos

#+BIND: org-export-filter-final-output-functions (filtro-html-misc)
#+begin_src emacs-lisp :exports results :results none
 ;; Para html
  (defun filtro-html-misc (texto backend info)
    "modifica epígrafe de las notas y añade un escalado menor a los
      números romanos, para emular algo parecido a versalitas"
    (when (org-export-derived-backend-p backend 'html)
      (replace-regexp-in-string ">Nota al pie de p&aacute;gina:"  ">Notas:"
      (replace-regexp-in-string "<p>Última actualización: "  (concat "<p>Última actualización: " (format "%s" (format-time-string "%d/%m/%y")))
      (replace-regexp-in-string "v-\\([IVXLCDM]+\\)-v"  "<font style=\"font-size:smaller;\">\\1</font>" texto)))))
#+end_src

Aunque la [[file:orgverselatex.html][salida a LaTeX desde Org Mode]] es siempre prioritaria para mí, de vez en cuando necesito exportar mis
escritos y notas en Org al formato ~odt~ de LibreOffice. La forma de hacerlo es bien sencilla. Tecleamos en Emacs la secuencia
~C-e o-o~ y ya tenemos listo al momento nuestro documento ~odt~. Para controlar los estilos en el proceso de
exportación, podemos también definir una plantilla de LibreOffice a tal efecto (o un archivo ~*.xml~ que contenga todos
esos estilos), y almacenarla donde queramos para invocarla luego, si añadimos al encabezado de nuestro documento Org
esta línea (comillas incluidas):

#+ATTR_HTML: :style background-color:#fdf6e3;
#+BEGIN_EXAMPLE
#+ODT_STYLES_FILE: "<ruta-hacia>/mi-plantilla.ott"
#+END_EXAMPLE

o esta otra, en caso de que usemos un archivo ~*.xml~

#+ATTR_HTML: :style background-color:#fdf6e3;
#+BEGIN_EXAMPLE
#+ODT_STYLES_FILE: "<ruta-hacia>/mi-hoja-de-estilos.xml"
#+END_EXAMPLE

Lo más sencillo, naturalmente, es crear la plantilla con LibreOffice. De esta forma:

1) Lanzamos el exportador desde Emacs, con nuestro documento Org abierto, y abrimos el archivo ~odt~ resultante.
2) Modificamos todos los estilos a nuestro gusto, tanto los predefinidos en LibreOffice como los que añade Org (cuyo
   nombre comenzará siempre por Org- y «lo que sea», como «OrgVerse», «OrgTitle», etc).
3) Guardamos el documento como plantilla (extensión ~*.ott~), para luego invocarla de la forma que se explicó más
   arriba.

Dicho todo esto, cabría añadir algo. El exportador a ~odt~ que viene por defecto en Org no va muy fino que digamos. Yo
lo he probado en la versión de Org más reciente, la 9.1.14, y algunas anteriores, tanto en Arch Linux como en Fedora, y
no hace algunas de las cosas que se le espera que hagan. Por ejemplo, no he sido capaz de añadirle a un bloque Org
estilos personalizados de ~odt~, salvo si se escribe código en bruto ~xml~ de LibreOffice, lo cual no es que sea muy
agradable. Aparte de que luego debemos comentar ese código si queremos exportar a otros formatos, como LaTeX o
~html~. Pero, investigando un poco, he encontrado en GitHub este [[https://github.com/kjambunathan/org-mode-ox-odt][«fork autorizado»]] del exportador de Org a ~odt~,
escrito por el propio autor del exportador «oficial». Hay de transfondo una serie de cuestiones que no alcanzo a
comprender, sobre todo por qué no se añade esta versión del exportador (máxime, cuando el autor es el mismo) a la
distribución oficial de Org en Emacs, porque el caso es que funciona a las mil maravillas. Para usarlo, no hace falta
instalar todo el repositorio, sino simplemente el exportador a ~odt~. Sería suficiente, en principio, con hacer lo
siguiente:

1) Clonar el repositorio en el directorio local que se nos antoje.
2) Entrar en el directorio ~/lisp~, buscar un archivo llamado ~ox-odt.el~ y copiarlo.
3) Crear (si no existe ya) en nuestra carpeta local ~~/.emacs.d~ un directorio llamado ~lisp/~ y pegar allí ese archivo.
4) Incluir, hacia el principio de nuestro archivo de configuración ~~/.emacs~ la línea:
#+BEGIN_SRC emacs-lisp
(add-to-list 'load-path "~/.emacs.d/lisp/")
#+END_SRC

Esto bastaría en principio ---insisto--- para que, cada vez que pulsemos la secuencia ~C-e o-o~, Emacs acuda al nuevo
exportador y no al que lleva «de fábrica». De esta forma podremos, entre otras cosas, añadir a un párrafo o (mejor) a un
bloque de Org un estilo personalizado de LibreOffice. Baste un ejemplo práctico. En Org suelo usar un bloque que he
llamado «lema», para situar allí las citas o dedicatorias que pueden encabezar un texto o un poema y que,
tipográficamente hablando, se alinean a la derecha de la página. En la exportación a LaTeX este bloque «lema» se
convierte en el entorno «lema», que ya tengo definido en una plantilla LaTeX[fn:2]. Si mi documento Org lo
quiero tener listo también para exportar a ~odt~, simplemente coloco antes del bloque un atributo ~#+ATTR_ODT~ que llame
a un estilo de LibreOffice ya contenido en mi plantilla personalizada. Algo así como esto:

#+ATTR_HTML: :style background-color:#fdf6e3;
#+BEGIN_EXAMPLE
#+ATTR_ODT: :style "lema"
#+begin_lema
Una variación del pesimismo
#+end_lema
#+END_EXAMPLE

Si queremos encabezar con una cita más larga, tal vez un poema, podemos también echar mano del bloque ~VERSE~:

#+ATTR_HTML: :style background-color:#fdf6e3;
#+BEGIN_EXAMPLE
#+ATTR_ODT: :style "lema"
#+begin_lema
#+BEGIN_VERSE
La princesa está triste... ¿Qué tendrá la princesa?
Los suspiros se escapan de su boca de fresa,
que ha perdido la risa, que ha perdido el color.
La princesa está pálida en su silla de oro,
está mudo el teclado de su clave sonoro,
y en un vaso, olvidada, se desmaya una flor.

El jardín puebla el triunfo de los pavos reales.
Parlanchina, la dueña dice cosas banales,
y vestido de rojo piruetea el bufón.
La princesa no ríe, la princesa no siente;
la princesa persigue por el cielo de Oriente
la libélula vaga de una vaga ilusión.
#+END_VERSE
#+end_lema
#+END_EXAMPLE

[fn:2] El exportador nativo de Org a LaTeX no sólo convierte en entornos los bloques predefinidos de Org, sino también
cualquier otro cuyo nombre nos inventemos sobre la marcha, lo cual es bastante útil. Nuestro nuevo entorno,
naturalmente, tendrá que estar definido en el preámbulo o la plantilla LaTeX que usemos para compilar.

* ∞
  :PROPERTIES:
  :ID:       8219f90e-fa0f-45a3-a701-ce170c4d4f10
  :PUBDATE:  <2019-07-14 dom 19:47>
  :END:

#+begin_export html
<div>
<p>Publicado: 02/10/2018 </p>

<p>Última actualización: </p>
</div>
<div>
<hr />
<p>
<a href="index.html">Índice general</a>
</p>
<p>
<a href="acerca-de.html">Acerca de...</a>
</p>
<p>
Esta obra está bajo una <a rel="license" href="http://creativecommons.org/licenses/by-nc/4.0/">licencia de Creative Commons Reconocimiento-NoComercial 4.0 Internacional</a>.
</p>
</div>
#+end_export
