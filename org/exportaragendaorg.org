#+TITLE: Exportar la agenda de Org a un mail automático y periódico
#+AUTHOR: Juan Manuel Macías
#+SETUPFILE: ~/Git/gnutas/html-gnutas.setup
#+OPTIONS: num:nil
#+OPTIONS: toc:nil
#+OPTIONS: tags:nil
#+OPTIONS: d:nil
#+LANGUAGE: es
#+OPTIONS: ':t
#+STARTUP: inlineimages
#+OPTIONS: todo:nil
#+STARTUP: fnlocal
#+INDEX: GNU Emacs!org-mode!agenda
#+INDEX: Bash!Cron


La agenda de Org Mode es un portento de eficiencia. Siempre solícita cada vez que la invoquemos, por ejemplo con la
secuencia ~C-c a a~, para que nos recolecte y muestre todos los ~TODO~ que tenemos en el día presente y en los que se
aproximan. Tareas, en fin, que temerariamente marcamos en el pasado con una fecha límite, y tan lejana nos parecía en su
momento. A la agenda, memoriosa, no se le escapa nada.

Pero si la agenda es casi perfecta en su asepsia, nosotros por contra resultamos indolentes, perezosos y (ese término
horrible, que ya pronunciarlo o escribirlo es una penitencia) procrastinadores natos. Es aquí donde dos venerables
herramientas, GNU Mail y Cron, se alían para convertirse en nuestra conciencia. Si no vamos a la agenda, hagamos que la
agenda vaya a nosotros y nos persiga. La idea es exportarla a un documento de texto simple, semanalmente, y
auto-enviarnos un correo periódico. Y la manera de hacerlo, muy sencilla, como veremos a continuación.

Para exportar la agenda, echamos mano de nuevo de la utilísima función ~batch~ de Emacs, que nos permite controlarlo a
distancia, casi con el pensamiento. Podemos usar este comando:

 #+begin_src bash
emacs -batch -l ~/.emacs -eval '(org-batch-agenda "a")' 2 > agenda.txt
 #+end_src

Lo que viene a hacer es evaluar en el archivo de inicio la función que muestra la vista de agenda y exporta el resultado
a un archivo de texto simple llamado ~agenda.txt~.

Creamos, a continuación, un sencillo /script/ que haga tres cosas:

1) Ejecutar ese comando
2) Enviarnos con GNU Mail el archivo ~agenda.txt~ (con la fecha actualizada) a nuestra dirección de correo
3) Eliminar el archivo ~agenda.txt~

#+begin_src bash
#!/bin/bash

emacs -batch -l ~/.emacs -eval '(org-batch-agenda "a")' 2 > agenda.txt

/usr/bin/mail -s "Agenda para $(date)" nuestra-dirección-de-correo < agenda.txt

rm agenda.txt

exit
#+end_src

Naturalmente, antes tendremos que configurar GNU Mail para que pueda enviar correos desde nuestra cuenta. Es tan simple
como introducir algunos datos en el archivo ~/etc/mail.rc~. Para ello, recomiendo echar un ojo a [[https://victorhckinthefreeworld.com/2014/04/23/enviar-correo-desde-la-linea-de-comandos-con-mail/][esta entrada de este
excelente blog]] donde se explica todo de forma meridiana.

Por último, asignamos la ejecución de nuestro /script/ (que hemos llamado ~agenda.sh~ y le hemos dado permisos de
ejecución) a una tarea programada con cron. La cuestión es: ¿cada cuanto tiempo? Bueno, no es cosa de que se nos
recuerde nuestras vergüenzas de hora en hora. Semanalmente me parece lo más razonable. Editamos crontab con el comando

 #+begin_src bash
crontab -e
 #+end_src

Y añadimos un comodín ~@weekly~ para no complicarnos con el jaleo de los asteriscos:

#+begin_src bash
@weekly /ruta/hacia/agenda.sh
#+end_src

Esto quiere decir que el /script/ se ejecutará todos los domingos a las 00:00. Hay una alta probabilidad de que a esas
horas esté con el ordenador encendido y reciba el mail. Pero, si no, tampoco pasa nada. Quién se va a enterar...

* ∞
  :PROPERTIES:
  :ID:       d9397904-b0ea-4796-92c1-570b5c7fe18b
  :PUBDATE:  <2019-07-14 dom 19:47>
  :END:

#+begin_export html
<div>
<p>Publicado: 10/03/2019 </p>

<p>Última actualización: 10/03/2019 </p>
</div>
<div>
<hr />
<p>
<a href="index.html">Índice general</a>
</p>
<p>
<a href="acerca-de.html">Acerca de...</a>
</p>
<p>
Esta obra está bajo una <a rel="license" href="http://creativecommons.org/licenses/by-nc/4.0/">licencia de Creative Commons Reconocimiento-NoComercial 4.0 Internacional</a>.
</p>
</div>
#+end_export
